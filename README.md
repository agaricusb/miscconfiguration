# MiscConfiguration

A mod to allow easy configuration of various miscellaneous values in Minecraft, currently:

* Add new shapeless crafting recipes
* Add new fuel burn times
* Change item max stack sizes
* Change block blast resistances

Downloads of latest development builds available at Buildhive: [![Build Status](https://buildhive.cloudbees.com/job/agaricusb/job/MiscConfiguration/badge/icon)](https://buildhive.cloudbees.com/job/agaricusb/job/MiscConfiguration/)

